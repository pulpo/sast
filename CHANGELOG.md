# GitLab SAST changelog

GitLab SAST follows versioning of GitLab (`MAJOR.MINOR` only) and generates a `MAJOR-MINOR-stable` [Docker image](https://gitlab.com/gitlab-org/security-products/sast/container_registry).

These "stable" Docker images may be updated after release date, changes are added to the corresponding section bellow.


## 10-5-stable
- Change versioning and release process
- Add Gemnasium analyzer to check dependencies for various languages and package managers:
  * **Ruby**: rubygems.
  * **Javascript**: npm and yarn.
  * **PHP**: composer (with the _packagist_ registry).
  * **Python**: pip (with the _pypi_ registry). Warning: need to update `sast` job config in `.gitlab-ci.yml`.
  * **Java**: maven (with the _central_ repository). Warning: need to update `sast` job config in `.gitlab-ci.yml`.
- Add support for projects with multiple languages (execute all matching analyzers).
- Allow to disable analyzers that upload data to GitLab central server using `SAST_DISABLE_REMOTE_CHECKS` env variable.
- Fix priority for RetireJS issues.
- Fix generated file path for RetireJS issues.
- Dedupe issues comming from different tools and having the same CVE
- Rename `CONFIDENCE_LEVEL` env variable to `SAST_CONFIDENCE_LEVEL` (shows a deprecation warning if old name is still used).

**Warning:** an update of the `sast` job config in `.gitlab-ci.yml` is necessary to benefit from the latest features, please check the [documentation](https://docs.gitlab.com/ee/ci/examples/sast.html).

## v0.3.0
- Fix CONFIDENCE_LEVEL env variable ignoring string value

## v0.3.0
- Allow pass CONFIDENCE_LEVEL env variable (works only with brakeman for now).

## v0.2.0
- Added Bandit, a python scanner.
- Sort issues in a report by priority.
