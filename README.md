# GitLab SAST

[![pipeline status](https://gitlab.com/gitlab-org/security-products/sast/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/sast/commits/master)
[![coverage report](https://gitlab.com/gitlab-org/security-products/sast/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/sast/commits/master)

GitLab tool for running Static Application Security Testing (SAST) on provided
source code.

## How to use

1. `cd` into the directory of the source code you want to scan
1. Run the Docker image:

    ```sh
    docker run \
      --interactive --tty --rm \
      --volume "$PWD":/code \
      --volume /var/run/docker.sock:/var/run/docker.sock \
      registry.gitlab.com/gitlab-org/security-products/sast:SAST_VERSION /app/bin/run /code
    ```
    Please replace `SAST_VERSION` with the latest available release matching your GitLab version. See [Versioning](#versioning-and-release-cycle) for more details.

1. The results will be stored in `gl-sast-report.json`

**Why mounting the Docker socket?**

Some tools require to be able to launch Docker containers to scan your application. You can skip this but you won't benefit from all scanners.

## Development

Running application:

```sh
# With Docker
docker run \
  --interactive --tty --rm \
  --volume "$PWD":/app \
  --volume /path/to/source/code:/code \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  ruby:latest /app/bin/run /code

# Without Docker (not recommended)
./bin/run /path/to/source/code
```

Running tests

```sh
bundle install
bundle exec rspec spec
```

## Supported languages, package managers and frameworks

The following table shows which languages, package managers and frameworks are supported and which tools are used.

| Language (package managers) / framework | Scan tool |
| ---------------------- | --------- |
| JavaScript ([npm](https://www.npmjs.com/), [yarn](https://yarnpkg.com/en/)) | [gemnasium](https://gitlab.com/gitlab-org/security-products/gemnasium), [Retire.js](https://retirejs.github.io/retire.js)
| Python ([pip](https://pip.pypa.io/en/stable/)) | [gemnasium](https://gitlab.com/gitlab-org/security-products/gemnasium), [bandit](https://github.com/openstack/bandit) |
| Ruby ([gem](https://rubygems.org/)) | [gemnasium](https://gitlab.com/gitlab-org/security-products/gemnasium), [bundler-audit](https://github.com/rubysec/bundler-audit) |
| Ruby on Rails | [brakeman](https://brakemanscanner.org) |
| Java ([Maven](http://maven.apache.org/)) | [gemnasium](https://gitlab.com/gitlab-org/security-products/gemnasium) |
| PHP ([Composer](https://getcomposer.org/)) | [gemnasium](https://gitlab.com/gitlab-org/security-products/gemnasium) |

## Remote checks

While some tools pull a local database to check vulnerabilities, some others require to send data to GitLab central servers to analyze them.
You can disable these tools by using the `SAST_DISABLE_REMOTE_CHECKS` [environment variable](https://docs.gitlab.com/ee/ci/variables/README.html#gitlab-ci-yml-defined-variables).

Here is the list of tools that are doing such remote checks and what kind of data they send:

**Gemnasium**

* Gemnasium scans the dependencies of your project locally and sends a list of packages to GitLab central servers.
* The servers return the list of known vulnerabilities for all the versions of these packages
* Then the client picks up the relevant vulnerabilities by comparing with the versions of the packages that are used by the project.

Gemnasium does *NOT* send the exact package versions your project relies on.

## Versioning and release cycle

GitLab SAST follows the versioning of GitLab (`MAJOR.MINOR` only) and is available as a Docker image tagged with `MAJOR-MINOR-stable`.

E.g. For GitLab `10.5.x` you'll need to run the `10-5-stable` GitLab SAST image:

    registry.gitlab.com/gitlab-org/security-products/sast:10-5-stable

Please note that the Auto-DevOps feature automatically uses the correct version. If you have your own `.gitlab-ci.yml` in your project, please ensure you are up-to-date with the [Auto-DevOps template](https://gitlab.com/gitlab-org/gitlab-ci-yml/blob/master/Auto-DevOps.gitlab-ci.yml).

# Contributing

If you want to help and extend the list of supported scanners, read the
[contribution guidelines](CONTRIBUTING.md).
