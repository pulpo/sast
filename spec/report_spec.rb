require_relative '../lib/report'
require_relative '../lib/issue'
require 'spec_helper'

RSpec.describe Report do
  describe '#dump' do
    let(:issues) do
      issues = [Issue.new, Issue.new, Issue.new]
      issues[0].priority = 'Low'
      issues[0].message = 'Low priority security issue'
      issues[1].priority = 'High'
      issues[1].message = 'High priority security issue'
      issues[2].priority = nil
      issues[2].message = 'Undefined priority security issue'

      Report.new(issues).dump
    end

    it { expect(JSON[issues][0]['priority']).to eq('High') }
  end

  describe '#dump with duplicates' do
    let(:issues) do
      issues = [Issue.new, Issue.new, Issue.new]
      issues[0].cve = '123'
      issues[0].tool = :gemnasium
      issues[0].message = 'Security issue from gemnasium'
      issues[1].priority = 'High'
      issues[1].cve = '123'
      issues[1].tool = :bundler_audit
      issues[1].message = 'High priority security issue from bundler audit'
      issues[2].priority = nil
      issues[2].tool = :bundler_audit
      issues[2].message = 'Undefined priority security issue from bundler audit'

      Report.new(issues).dump
    end

    it 'expect to have deduped issues with same CVE' do
      dump = JSON[issues]
      expect(dump.size).to eq(2)
      expect(dump[0]['tool']).to eq('bundler_audit')
      expect(dump[0]['tools']).to eq(%w[bundler_audit gemnasium])
    end
  end
end
