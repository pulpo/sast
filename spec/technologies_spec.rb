require_relative '../lib/technologies'
require 'rspec'

RSpec.describe Technologies do
  let(:technologies) do
    Technologies.detect_technologies(app_path)
  end

  context '"with a Rails app' do
    let!(:app_path) { clone_rails_app }

    it 'detects the Ruby, Bundler and Rails technology' do
      expect(technologies).to satisfy { |ts|
        ts.technology?(Technology.new(:ruby, :bundler, :rails))
      }
    end
  end

  context 'with a Javascript NPM app' do
    let!(:app_path) { clone_js_npm_app }

    it 'detects the Javascript and NPM technology' do
      expect(technologies).to satisfy { |ts|
        ts.technology?(Technology.new(:js, :npm))
      }
    end
  end

  context 'with a Javascript YARN app' do
    let!(:app_path) { clone_js_yarn_app }

    it 'detects the Javascript and YARN technology' do
      expect(technologies).to satisfy { |ts|
        ts.technology?(Technology.new(:js, :yarn))
      }
    end
  end

  context 'with a Python Pip app' do
    let!(:app_path) { clone_py_app }

    it 'detects the Python and Pip technology' do
      expect(technologies).to satisfy { |ts|
        ts.technology?(Technology.new(:python, :pip))
      }
    end
  end
end
