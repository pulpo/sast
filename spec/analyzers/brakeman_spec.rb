require_relative '../../lib/analyzers/brakeman'
require 'spec_helper'

RSpec.describe Analyzers::Brakeman do
  let!(:app_path) { clone_rails_app }
  let(:app) { double(technologies: rails_techs, path: app_path) }

  let(:issues) do
    Bundler.with_clean_env do
      Analyzers::Brakeman.new(app).execute
    end
  end

  it 'expect to have correct issues' do
    expect(issues.size).to eq(1)
    expect(issues.first.tool).to eq(:brakeman)
    expect(issues.first.message).to eq('Possible command injection')
  end

  context 'SAST_CONFIDENCE_LEVEL=2' do
    before do
      allow(ENV).to receive(:[]).with('SAST_CONFIDENCE_LEVEL').and_return('2')
    end

    it 'expect to have correct issues' do
      expect(issues.size).to eq(2)
      expect(issues.last.tool).to eq(:brakeman)
      expect(issues.last.message).to eq('Possible SQL injection')
    end
  end
end
