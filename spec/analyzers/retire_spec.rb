require_relative '../../lib/analyzers/retire'
require 'spec_helper'

RSpec.describe Analyzers::Retire do
  let!(:app_path) { clone_js_npm_app }
  let(:app) { double(technologies: js_npm_techs, path: app_path) }

  let(:issues) do
    Bundler.with_clean_env do
      Analyzers::Retire.new(app).execute
    end
  end

  it 'expect to have correct issues' do
    expect(issues.size).to eq(1)
    expect(issues[0].priority).to eq('High')
    expect(issues[0].tool).to eq(:retire)
    expect(issues[0].url).to eq('https://nodesecurity.io/advisories/51')
  end
end
