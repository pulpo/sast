require_relative '../../lib/analyzers/gemnasium'
require 'spec_helper'

RSpec.describe Analyzers::Gemnasium do
  let!(:app_path) { clone_js_yarn_app }
  let(:app) { double(technologies: js_yarn_techs, path: app_path) }

  let(:issues) do
    Bundler.with_clean_env do
      Analyzers::Gemnasium.new(app).execute
    end
  end

  it 'expect to have correct issues' do
    expect(issues.size).to be >= 9
    expect(issues[0].tool).to eq(:gemnasium)
    expect(issues[0].message).to eq('Regular Expression Denial of Service for minimatch')
  end
end
