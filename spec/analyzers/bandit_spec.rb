require_relative '../../lib/analyzers/bandit'
require 'spec_helper'

RSpec.describe Analyzers::Bandit do
  let!(:app_path) { clone_py_app }
  let(:app) { double(technologies: python_techs, path: app_path) }

  let(:issues) do
    Bundler.with_clean_env do
      Analyzers::Bandit.new(app).execute
    end
  end

  it 'expect to have correct issues' do
    expect(issues.size).to eq(83)
    expect(issues[2].tool).to eq(:bandit)
    expect(issues[2].message).to eq('Use of assert detected. The enclosed code will be removed when compiling to optimised byte code.')
  end
end
