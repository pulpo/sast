require_relative '../../lib/analyzers/bundle_audit'
require 'spec_helper'

RSpec.describe Analyzers::BundleAudit do
  let(:issues) do
    Bundler.with_clean_env do
      Analyzers::BundleAudit.new(app).execute
    end
  end

  context 'rails app, 1 vulnerability found' do
    let!(:app_path) { clone_rails_app }
    let(:app) { double(technologies: rails_techs, path: app_path) }

    it 'expect to have correct issues' do
      expect(issues.size).to eq(2)
      expect(issues[1].tool).to eq(:bundler_audit)
      expect(issues[1] .message).to eq('uglifier incorrectly handles non-boolean comparisons during minification')
    end
  end

  context 'ruby app, no vulnerabilities found' do
    let!(:app_path) { clone_ruby_app }
    let(:app) { double(technologies: ruby_techs, path: app_path) }

    it 'handles empty output well' do
      expect(issues.size).to eq(0)
    end
  end
end
