require 'json'

# Dump issues to json and save as file
class Report
  attr_reader :issues

  def initialize(issues)
    @issues = issues
  end

  def save_as(target_path)
    File.write(target_path, dump)
    puts "SUCCESS: Report saved in #{target_path}"
  end

  def dump
    priority = %w[High Medium Low Unknown].freeze

    dedupe_issues!(@issues)
    @issues.sort_by! { |issue| priority.index(issue.priority) || priority.size }
    @issues.map(&:to_hash).to_json
  end

  private

  # Avoid reporting the same issue from multiple tools
  # Dedupe based on the CVE identifier only for now
  def dedupe_issues!(issues)
    # Tools with best metadata come first
    tools_priority = %i[bundler_audit retire gemnasium].freeze

    issues.group_by(&:cve).each do |cve, duplicates|
      next unless cve

      # Keep the issue from the tool with best metadata
      duplicates.sort_by! { |dup| tools_priority.index(dup.tool) || 999 }
      first = duplicates.shift

      # Agregates all tools in a new property
      first.tools = [first.tool]
      duplicates.each do |dup|
        first.tools << dup.tool
        # Remove duplicate
        issues.delete dup
      end
    end
  end
end
