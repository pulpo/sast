require 'set'
require 'forwardable'
require_relative 'technology'

# A set of technologies and convenient methods to query it
class Technologies
  extend Forwardable

  attr_reader :technologies

  def_delegators :@technologies, :add

  def initialize
    @technologies = Set.new([])
  end

  def any?
    @technologies.any?
  end

  def language?(name)
    @technologies.any? { |t| t.language?(name) }
  end

  def package_manager?(name)
    @technologies.any? { |t| t.package_manager?(name) }
  end

  def framework?(name)
    @technologies.any? { |t| t.framework?(name) }
  end

  def technology?(tech)
    @technologies.include?(tech)
  end

  # Given the path of a project, detect technologies used in the project
  def self.detect_technologies(path)
    techs = Technologies.new
    files = Dir.entries(path)

    if files.include?('Gemfile.lock')
      techs.add(Technology.ruby_bundler)
      # check for Rails
      content = File.read(File.join(path, 'Gemfile.lock'))
      techs.add(Technology.rails) if content.include?(' rails ')
    end

    if files.include?('yarn.lock')
      techs.add(Technology.js_yarn)
    elsif files.include?('package.json') # consider it's npm (may use package-lock.json or npm-shrinkwrap.json)
      techs.add(Technology.js_npm)
    end

    if files.include?('setup.py') || files.include?('Pipfile') || files.include?('requires.txt') || files.include?('requirements.txt') || files.include?('requirements.pip')
      techs.add(Technology.python_pip)
    end

    techs.add(Technology.php_composer) if files.include?('composer.lock')

    techs.add(Technology.java_maven) if files.include?('pom.xml')

    techs
  end
end
