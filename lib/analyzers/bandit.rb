require 'json'

require_relative 'helpers'
require_relative '../issue'

module Analyzers
  # Language: Python
  # Framework: Any
  # Python AST-based static analyzer from OpenStack Security Group
  class Bandit
    include Analyzers::Helpers

    REPORT_NAME = 'gl-sast-bandit.json'.freeze

    attr_reader :app, :report_path

    def initialize(app)
      @app = app
      @report_path = File.join(@app.path, REPORT_NAME)
    end

    def execute
      install_python
      output = analyze
      output_to_issues(output)
    end

    private

    def analyze
      Dir.chdir(@app.path) do
        cmd <<-SH
          pip install bandit
          bandit -a vuln -f json -o #{report_path} -r .
        SH

        JSON.parse(File.read(report_path))
      end
    ensure
      File.delete(report_path) if File.exist?(report_path)
    end

    def output_to_issues(output)
      issues = []

      output['results'].each do |warning|
        issue = Issue.new
        issue.tool = :bandit
        issue.file = warning['filename']
        issue.message = warning['issue_text']
        issue.priority = warning['issue_severity']
        issue.line = warning['line_number']
        issues << issue
      end

      issues
    end

    def install_python
      cmd <<-SH
        apt-get update && apt-get install -y python libpython-dev python-pip
      SH
    end
  end
end
